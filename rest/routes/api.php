<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('picture/{location}', 'PictureController@getByLocation');
Route::get('text/{location}', 'TextController@getByLocation');
Route::get('category', 'CategoryController@getCategories');
Route::post('category', 'CategoryController@createCategory');
Route::put('category/{categoryId}', 'CategoryController@updateCategory');
Route::delete('category/{categoryId}', 'CategoryController@deleteCategory');
