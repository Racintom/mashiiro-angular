<?php


namespace App\Http\Controllers;


use App\Database\PictureDao;
use Illuminate\Support\Facades\Response;

class PictureController extends Controller {

    private $pictureDao;

    public function __construct() {
        $this->pictureDao = new PictureDao();
    }

    public function getByLocation(string $location) {
        return Response::json($this->pictureDao->getByLocation($location));
    }


}
