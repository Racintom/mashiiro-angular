<?php


namespace App\Http\Controllers;


use App\Database\TextDao;
use Illuminate\Support\Facades\Response;

class TextController extends Controller {

    private $textDao;

    public function __construct() {
        $this->textDao = new TextDao();
    }

    public function getByLocation(string $location) {
        return Response::json($this->textDao->getByLocation($location));
    }
}
