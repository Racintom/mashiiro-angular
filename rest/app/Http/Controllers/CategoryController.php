<?php


namespace App\Http\Controllers;


use App\Database\CategoryDao;
use App\Services\PictureService;
use Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class CategoryController extends Controller {

    private $categoryDao;
    private $pictureService;

    public function __construct() {
        $this->categoryDao = new CategoryDao();
        $this->pictureService = new PictureService();
    }

    public function getCategories() {
        return Response::json($this->categoryDao->getCategories());
    }

    public function deleteCategory() {
        // todo: implement lol
    }

    public function createCategory(Request $request) {
        $name = $request->input('name');
        return Response::json($request->hasFile('p'));
        // $this->pictureService->handleUploadFile($picture);

        //return Response::json($this->categoryDao->createCategory(new Category($name, $path)));
    }

    public function updateCategory(Request $request) {
        $name = $request->input('name');
        $path = $request->input('pathPortfolioMainPicture');
        return Response::json($this->categoryDao->updateCategory(new Category($name, $path)));
    }

}
