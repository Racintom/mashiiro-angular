<?php

namespace App\Services;

use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class PictureService {

    const resolutions = [80, 400, 768, 1024, 1400, 2100];
    const dir = '../../resources/upload/';
    const extension = '.jpg';

    public function handleUploadFile(UploadedFile $file): string {
        Image::make($file)->save('../../resources/upload');

        /*        $hash = uniqid('', true) . '.';

        foreach (self::resolutions as $resolution) {
            $path = self::dir;
            $path .= $hash . $resolution . self::extension;

            $this->resizeImage($file, $resolution)->save($path, 95, Image::JPEG);

            /*    if (!file_exists($path)) {
                    throw new Exception("Obrázek se neuložil. Prosím, opakujte akci");
                }
            */

/*        }*/

    }

    private function saveFileOnDisk(): string {

    }

    private function resizeImage(UploadedFile $file, int $resolution): Image {
        return $file->toImage()->resize($resolution, null);
    }


    private function saveFileInResolution(): void {

    }
}
