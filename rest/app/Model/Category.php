<?php


class Category {
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $pathPortfolioMainPicture;

    public function __construct(string $name, string $pathPortfolioMainPicture) {
        $this->name = $name;
        $this->pathPortfolioMainPicture = $pathPortfolioMainPicture;
    }


}
