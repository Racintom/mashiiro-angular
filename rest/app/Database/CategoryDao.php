<?php


namespace App\Database;


use Category;
use PDO;

class CategoryDao extends BaseDao {

    public function getCategories() {
        $query = $this->connection->prepare("SELECT * FROM categories");
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function createCategory(Category $category) {
        $query = $this->connection->prepare("INSERT INTO categories (name, portfolio_main_photo) VALUE (?, ?)");
        return $query->execute([$category->name, $category->pathPortfolioMainPicture]);
    }

    public function deleteCategory(int $id) {
        $query = $this->connection->prepare("DELETE FROM categories WHERE ID = ?");
        return $query->execute([$id]);
    }

    public function updateCategory(Category $category) {
        $query = $this->connection->prepare("UPDATE categories SET name = ?, portfolio_main_photo = ? WHERE ID = ?");
        return $query->execute([$category->name, $category->pathPortfolioMainPicture, $category->id]);
    }
}
