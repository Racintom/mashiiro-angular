<?php


namespace App\Database;


abstract class BaseDao {

    /**
     * @var \PDO
     */
    protected $connection;

    public function __construct() {
        $this->connection = (new Connection())->get();
    }
}
