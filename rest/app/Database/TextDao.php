<?php


namespace App\Database;


use PDO;

class TextDao extends BaseDao {

    public function getByLocation(string $location) {
        $query = $this->connection->prepare("SELECT * FROM texts WHERE location = :location");
        $query->execute([
            'location' => $location
        ]);
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return count($result) > 0 ? $result[0] : [];
    }
}
