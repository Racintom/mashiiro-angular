<?php


namespace App\Database;


use PDO;    // todo: remodelovat databazi. Aby obrazk bez kategorie byly v jine tabulce, nez ty s categorii

class PictureDao extends BaseDao {

    public function getByLocation(string $location) {
        $query = $this->connection->prepare(
            "SELECT * FROM photographies
                      LEFT JOIN categories ON photographies.category = categories.ID
                      WHERE location = :location"
        );
        $query->execute([
           'location' => $location
        ]);
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
}
