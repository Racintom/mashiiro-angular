<?php


namespace App\Database;

define('SQL_HOST', 'localhost');
define('SQL_DBNAME', 'mashiiro');
define('SQL_USERNAME', 'root');
define('SQL_PASSWORD', '');


use PDO;
use PDOException;

class Connection {

    /**
     * @var PDO
     */
    private static $connection = null;

    public function __construct() {
        if (self::$connection == null) {
            self::$connection = $this->connect();
        }
    }

    private function connect(): PDO {
        $dsn = 'mysql:dbname=' . SQL_DBNAME . ';host=' . SQL_HOST . '';
        $user = SQL_USERNAME;
        $password = SQL_PASSWORD;

        try {
            $pdo = new PDO($dsn, $user, $password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->query("SET NAMES utf8");
        } catch (PDOException $e) {
            die('Connection failed: ' . $e->getMessage());
        }
        return $pdo;
    }

    public function get(): PDO {
        return self::$connection;
    }
}
