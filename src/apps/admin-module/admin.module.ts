import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardComponent} from './component/dashboard/dashboard.component';
import {SliderEditorComponent} from './component/slider/slider-editor/slider-editor.component';
import {HomepageEditorComponent} from './component/homepage-editor/homepage-editor.component';
import {SliderEditorNewPhotoComponent} from './component/slider/slider-editor-new-photo/slider-editor-new-photo.component';
import {SliderEditorEditPhotoComponent} from './component/slider/slider-editor-edit-photo/slider-editor-edit-photo.component';
import {AdminRoutingModule} from './admin-routing.module';
import {MainComponent} from './component/main/main.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AdminPhotosDisplayComponent} from './component/photo/admin-photos-display/admin-photos-display.component';
import {LoginComponent} from './component/login/login.component';
import {CategoryEditorComponent} from './component/category/category-editor/category-editor.component';
import {CategoryDisplayComponent} from './component/category/category-display/category-display.component';
import {DeleteEntityComponent} from './component/delete-entity/delete-entity.component';
import {PortfolioEditorComponent} from './component/portfolio/portfolio-editor/portfolio-editor.component';
import {PortfolioMainPhotosEditorComponent} from './component/portfolio/portfolio-main-photos-editor/portfolio-main-photos-editor.component';


@NgModule({
  declarations: [DashboardComponent, SliderEditorComponent, HomepageEditorComponent,
    SliderEditorNewPhotoComponent, SliderEditorEditPhotoComponent, MainComponent, AdminPhotosDisplayComponent,
   LoginComponent,
   CategoryEditorComponent,
   CategoryDisplayComponent,
   DeleteEntityComponent,
   PortfolioEditorComponent,
   PortfolioMainPhotosEditorComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class AdminModule { }
