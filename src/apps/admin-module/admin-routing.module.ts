import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {HomepageEditorComponent} from './component/homepage-editor/homepage-editor.component';
import {SliderEditorComponent} from './component/slider/slider-editor/slider-editor.component';
import {DashboardComponent} from './component/dashboard/dashboard.component';
import {MainComponent} from './component/main/main.component';
import {CategoryEditorComponent} from './component/category/category-editor/category-editor.component';
import {PortfolioEditorComponent} from './component/portfolio/portfolio-editor/portfolio-editor.component';

// todo: add login guard
const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'homepage',
    component: HomepageEditorComponent,
  },
  {
    path: 'homepage/slider',
    component: SliderEditorComponent,
  },
  {
    path: 'category',
    component: CategoryEditorComponent,
  },
  {
    path: 'portfolio',
    component: PortfolioEditorComponent,
  },
  {
    path: 'portfolio/main-photos',
    component: PortfolioEditorComponent,
  },
  {
    path: 'portfolio/photos',
    component: PortfolioEditorComponent,
  },
  {
    path: '',
    component: MainComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
