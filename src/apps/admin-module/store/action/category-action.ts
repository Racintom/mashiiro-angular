import {Action, createAction} from '@ngrx/store';
import {Category} from '../../../shared-module/model';

export const ADD_CATEGORY = '[Category] addCategory';
export const ADDED_CATEGORY = '[Category] addedCategory';
export const LOADED_CATEGORY = '[Category] loadedCategory';
export const UPDATE_CATEGORY = '[Category] updateCategory';
export const UPDATED_CATEGORY = '[Category] updatedCategory';
export const DELETE_CATEGORY = '[Category] deleteCategory';
export const DELETED_CATEGORY = '[Category] deletedCategory';
export const GET_CATEGORY = '[Category] getCategory';

export const add = createAction(ADD_CATEGORY);
export const added = createAction(ADDED_CATEGORY);
export const remove = createAction(DELETE_CATEGORY);
export const removed = createAction(DELETED_CATEGORY);
export const loaded = createAction(LOADED_CATEGORY);
export const get = createAction(GET_CATEGORY);
export const update = createAction(UPDATE_CATEGORY);
export const updated = createAction(UPDATED_CATEGORY);

export class CategoryAddAction implements Action {
  readonly type = ADD_CATEGORY;

  constructor(public payload: any) {}
}

export class CategoryAddedAction implements Action {
  readonly type = ADDED_CATEGORY;

  constructor(public payload: Category) {}
}

export class CategoryGetAction implements Action {
  readonly type = GET_CATEGORY;
}

export class CategoryLoadedAction implements Action {
  readonly type = LOADED_CATEGORY;

  constructor(public payload?: Category[]) {}
}

export class CategoryDeleteAction implements Action {
  readonly type = DELETE_CATEGORY;

  constructor(public payload: Category) {}
}

export class CategoryDeletedAction implements Action {
  readonly type = DELETED_CATEGORY;

  constructor(public payload: Category) {}
}

export class CategoryUpdateAction implements Action {
  readonly type = UPDATE_CATEGORY;

  constructor(public payload: Category) {}
}

export class CategoryUpdatedAction implements Action {
  readonly type = UPDATED_CATEGORY;

  constructor(public payload: Category) {}
}
