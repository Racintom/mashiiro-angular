import {Action, createAction} from '@ngrx/store';
import {Picture} from '../../../shared-module/model';

export const ADD_PHOTO = '[Portfolio] addMainPhoto';
export const ADDED_PHOTO = '[Portfolio] addedMainPhoto';
export const LOADED_PHOTO = '[Portfolio] loadedMainPhoto';
export const UPDATE_PHOTO = '[Portfolio] updateMainPhoto';
export const UPDATED_PHOTO = '[Portfolio] updatedMainPhoto';
export const DELETE_PHOTO = '[Portfolio] deleteMainPhoto';
export const DELETED_PHOTO = '[Portfolio] deletedMainPhoto';
export const GET_PHOTO = '[Portfolio] getMainPhoto';

export const add = createAction(ADD_PHOTO);
export const added = createAction(ADDED_PHOTO);
export const remove = createAction(DELETE_PHOTO);
export const removed = createAction(DELETED_PHOTO);
export const loaded = createAction(LOADED_PHOTO);
export const get = createAction(GET_PHOTO);
export const update = createAction(UPDATE_PHOTO);
export const updated = createAction(UPDATED_PHOTO);

export class PhotoAddAction implements Action {
  readonly type = ADD_PHOTO;

  constructor(public payload: Picture) {}
}

export class PhotoAddedAction implements Action {
  readonly type = ADDED_PHOTO;

  constructor(public payload: Picture) {}
}

export class PhotoLoadedAction implements Action {
  readonly type = LOADED_PHOTO;

  constructor(public payload: Picture) {}
}
