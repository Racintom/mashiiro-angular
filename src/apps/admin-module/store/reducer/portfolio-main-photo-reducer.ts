import {createReducer, on} from '@ngrx/store';
import {Picture} from '../../../shared-module/model';
import {added, get, PhotoAddAction} from '../action/portfolio-main-photo-action';

const initialState = [];

export const portfolioMainPhoto = createReducer(initialState,
  on(added, addMainPhotoReducer),
//  on(removed, removePhotoReducer),
//  on(updated, updatePhotoReducer),
  on(get, state => state),
//  on(loaded, addPhotoReducer),
);

function addMainPhotoReducer(currentState: Picture[], action: PhotoAddAction): Picture[] {
  if (Array.isArray(action.payload)) {
    return currentState.concat(action.payload);
  }
  return [... currentState, action.payload];
}
/*
function removeCategoryReducer(currentState: Category[], action: PhotoDeletedAction): Picture[] {
  return currentState.filter(category => category.id !== action.payload.id);
}

function updateCategoryReducer(currentState: Category[], action: CategoryUpdatedAction): Picture[] {
  return currentState.map(category => {
    if (category.id === action.payload.id) {
      return action.payload;
    }
    return category;
  });
}*/
