import {createReducer, on} from '@ngrx/store';
import {
  added,
  CategoryAddedAction,
  CategoryDeletedAction,
  CategoryUpdatedAction,
  get,
  loaded,
  removed,
  updated
} from '../action/category-action';
import {Category} from '../../../shared-module/model';

const initialState = [];

export const categoryReducer = createReducer(initialState,
  on(added, addCategoryReducer),
  on(removed, removeCategoryReducer),
  on(updated, updateCategoryReducer),
  on(get, state => state),
  on(loaded, addCategoryReducer),
);

function addCategoryReducer(currentState: Category[], action: CategoryAddedAction): Category[] {
  if (Array.isArray(action.payload)) {
    return currentState.concat(action.payload);
  }
  return [... currentState, action.payload];
}

function removeCategoryReducer(currentState: Category[], action: CategoryDeletedAction): Category[] {
  return currentState.filter(category => category.id !== action.payload.id);
}

function updateCategoryReducer(currentState: Category[], action: CategoryUpdatedAction): Category[] {
  return currentState.map(category => {
    if (category.id === action.payload.id) {
      return action.payload;
    }
    return category;
  });
}
