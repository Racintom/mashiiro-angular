import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {CategoryDataService} from '../../../shared-module/data-service/category-data.service';
import {
  ADD_CATEGORY,
  CategoryAddAction,
  CategoryAddedAction,
  CategoryDeleteAction,
  CategoryDeletedAction,
  CategoryLoadedAction,
  CategoryUpdateAction,
  CategoryUpdatedAction,
  DELETE_CATEGORY,
  GET_CATEGORY,
  UPDATE_CATEGORY
} from '../action/category-action';
import {map, switchMap} from 'rxjs/operators';

@Injectable()
export class CategoryEffects {
  readonly categoryGet$ = createEffect(() => this.actions$.pipe(
    ofType(GET_CATEGORY),
    switchMap(() => this.categoryDataService.getCategories().pipe(
      map(categories => new CategoryLoadedAction(categories))
    ))
  ));

  readonly categoryDelete$ = createEffect(() => this.actions$.pipe(
    ofType(DELETE_CATEGORY),
    switchMap((action: CategoryDeleteAction) => this.categoryDataService.deleteCategory(action.payload.id).pipe(
      map(() => new CategoryDeletedAction(action.payload))
    ))
  ));

  readonly categoryUpdate$ = createEffect(() => this.actions$.pipe(
    ofType(UPDATE_CATEGORY),
    switchMap((action: CategoryUpdateAction) => this.categoryDataService.updateCategory(action.payload).pipe(
      map(() => new CategoryUpdatedAction(action.payload))
    ))
  ));

  readonly categoryAdd$ = createEffect(() => this.actions$.pipe(
    ofType(ADD_CATEGORY),
    switchMap((action: CategoryAddAction) => this.categoryDataService.addCategory(action.payload).pipe(
      map((path) => new CategoryAddedAction({
        name: /*action.payload.get('name').toString()*/'sdlk',
        pathPortfolioMainPicture: path
      }))
    ))
  ));

  constructor(
    private actions$: Actions,
    private categoryDataService: CategoryDataService
  ) {}
}
