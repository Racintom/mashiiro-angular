import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AdminPhotosDisplayComponent} from './admin-photos-display.component';

describe('AdminPhotosDisplayComponent', () => {
  let component: AdminPhotosDisplayComponent;
  let fixture: ComponentFixture<AdminPhotosDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPhotosDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPhotosDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
