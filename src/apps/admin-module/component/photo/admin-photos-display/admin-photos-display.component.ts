import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Picture} from '../../../../shared-module/model';

@Component({
  selector: 'ms-admin-photos-display',
  templateUrl: './admin-photos-display.component.html',
  styleUrls: ['./admin-photos-display.component.scss']
})
export class AdminPhotosDisplayComponent implements OnInit {

  @Input()
  pictures: Picture[];

  @Output()
  selectedPicture = new EventEmitter<Picture>();

  constructor() { }

  ngOnInit(): void {
  }

  emitSelectedPicture(picture: Picture) {
    this.selectedPicture.emit(picture);
  }

}
