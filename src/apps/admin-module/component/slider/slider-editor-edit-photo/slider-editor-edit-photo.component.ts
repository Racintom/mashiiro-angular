import {Component, Input, OnInit} from '@angular/core';
import {Picture} from '../../../../shared-module/model';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'ms-slider-editor-edit-photo',
  templateUrl: './slider-editor-edit-photo.component.html',
  styleUrls: ['./slider-editor-edit-photo.component.scss']
})
export class SliderEditorEditPhotoComponent implements OnInit {

  form: FormGroup;

  @Input()
  picture: Picture;

  constructor() { }

  ngOnInit(): void {
  }

}
