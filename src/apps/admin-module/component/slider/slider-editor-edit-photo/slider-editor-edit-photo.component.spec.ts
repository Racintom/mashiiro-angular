import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SliderEditorEditPhotoComponent} from './slider-editor-edit-photo.component';

describe('SliderEditorEditPhotoComponent', () => {
  let component: SliderEditorEditPhotoComponent;
  let fixture: ComponentFixture<SliderEditorEditPhotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SliderEditorEditPhotoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SliderEditorEditPhotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
