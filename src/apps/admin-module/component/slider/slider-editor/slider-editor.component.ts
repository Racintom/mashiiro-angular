import {Component, OnInit} from '@angular/core';
import {Category, Picture, PictureLocation} from '../../../../shared-module/model';
import {PictureService} from '../../../../shared-module/service/picture.service';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'ms-slider-editor',
  templateUrl: './slider-editor.component.html',
  styleUrls: ['./slider-editor.component.scss']
})
export class SliderEditorComponent implements OnInit {

  readonly pictures$ = this.service.getAllPictures(PictureLocation.HomepageSlider);
  selectedPicture: Picture;
  categories: Category[];

  constructor(
    private service: PictureService,
    private fb: FormBuilder,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.categories = this.route.snapshot.data.categories;
  }

  selectPicture(picture: Picture): void {
    this.selectedPicture = picture;
  }
}
