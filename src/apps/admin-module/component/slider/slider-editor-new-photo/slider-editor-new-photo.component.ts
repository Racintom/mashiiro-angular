import {Component, OnInit} from '@angular/core';
import {Category} from '../../../../shared-module/model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CategoryService} from '../../../../shared-module/service/category.service';
import {first} from 'rxjs/operators';
import {Store} from '@ngrx/store';
import {categoriesListSelector} from '../../../store/selector/category-selector';

@Component({
  selector: 'ms-slider-editor-new-photo',
  templateUrl: './slider-editor-new-photo.component.html',
  styleUrls: ['./slider-editor-new-photo.component.scss']
})
export class SliderEditorNewPhotoComponent implements OnInit {

  form: FormGroup;
  categories: Category[];
  loaded = false;

  constructor(
    private categoryService: CategoryService,
    private store: Store
  ) {}

  ngOnInit(): void {
    this.store.select(categoriesListSelector)
      .pipe(first(categories => categories != null))
      .subscribe(categories => {
        this.loaded = true;
        this.categories = categories;
        this.form = this.buildForm();
    });
  }

  submit() {
    console.log(this.form.value);
  }

  private buildForm(): FormGroup {
    return new FormGroup({
      title: new FormControl('', Validators.required),
      description: new FormControl('', Validators. required),
      category: new FormControl(undefined, Validators.required)
    });
  }

}
