import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SliderEditorNewPhotoComponent} from './slider-editor-new-photo.component';

describe('SliderEditorNewPhotoComponent', () => {
  let component: SliderEditorNewPhotoComponent;
  let fixture: ComponentFixture<SliderEditorNewPhotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SliderEditorNewPhotoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SliderEditorNewPhotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
