import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {LoginService} from './login.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'ms-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnDestroy {

  form: FormGroup = this.formBuilder.group({
    username: new FormControl('', this.validators),
    password: new FormControl('', this.validators)
  });

  private destroy$ = new Subject();

  constructor(
    private service: LoginService,
    private formBuilder: FormBuilder
    ) { }

  get validators(): ValidatorFn[] {
    return [Validators.required, Validators.minLength(6), Validators.maxLength(1024)];
  }

  submit() {
    const {username, password} = this.form.value;
    this.service.login(username, password).pipe(takeUntil(this.destroy$)).subscribe(success => {
      console.log(success);
      // todo: handle login
    });


  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }
}
