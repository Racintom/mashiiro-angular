import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Category} from '../../../../shared-module/model';

@Component({
  selector: 'ms-category-display',
  templateUrl: './category-display.component.html',
  styleUrls: ['./category-display.component.scss']
})
export class CategoryDisplayComponent implements OnInit {

  @Input()
  categories: Category[] = [];

  @Output()
  editCategory = new EventEmitter<Category>();

  @Output()
  deleteCategory = new EventEmitter<Category>();

  constructor() { }

  ngOnInit(): void {
  }

  editCategoryHandler(category: Category) {
    this.editCategory.emit(category);
  }

  deleteCategoryHandler(category: Category) {
    this.deleteCategory.emit(category);
  }

}
