import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {Category, FormMode} from '../../../../shared-module/model';
import {categoriesListSelector} from '../../../store/selector/category-selector';
import {CategoryAddAction, CategoryDeleteAction, CategoryGetAction} from '../../../store/action/category-action';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';

type HeaderDescription = 'Vytvoření nové kategorie' | 'Úprava existující fotografie';

@Component({
  selector: 'ms-category-editor',
  templateUrl: './category-editor.component.html',
  styleUrls: ['./category-editor.component.scss']
})
export class CategoryEditorComponent implements OnInit {

  uploadedFile: any;
  readonly categories$ = this.store.select(categoriesListSelector);
  formDescription: HeaderDescription;
  form: FormGroup;
  formMode: FormMode = 0;

  constructor(
    private store: Store,
    private fb: FormBuilder,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.buildForm();
    this.store.dispatch(new CategoryGetAction());
  }

  buildForm(): void {
    this.form = this.fb.group({
      name: new FormControl(null, {validators: [Validators.maxLength(50), Validators.required], updateOn: 'blur'}),
      picture: new FormControl(null, Validators.required)
    });
  }


  create(): void {
    const values = this.form.getRawValue();
    const input = new FormData();
    input.append('name', values.name);
    input.append('p', this.uploadedFile);
    this.store.dispatch(new CategoryAddAction(input));
  }

  onFileChange(event): void {
    this.uploadedFile = event.target.files[0];
  }

  deleteCategory(category: Category): void {
    // todo: add some popup if sure
    // todo: also delete all photos that are in the category
    this.store.dispatch(new CategoryDeleteAction(category));
  }

  editCategory(category: Category): void {
    this.switchToEditMode(category);
    // this.store.dispatch(new CategoryUpdateAction(category));
  }

  initializeForm(category: Category): void {
    this.form.controls.name.patchValue(category.name);
  }

  switchToEditMode(category: Category): void {
    this.formMode = FormMode.EDIT;
    this.formDescription = 'Úprava existující fotografie';

    this.initializeForm(category);
  }

  // todo: doladit s mashiiro
  // todo: dva navrhy. Budto se pod kategorii rozvine formular (pres service) nebo popupka
  switchToCreateMode(): void {
    this.form.controls.name.patchValue(null);
    this.formMode = FormMode.EDIT;
    this.formDescription = 'Vytvoření nové kategorie';
  }

}
