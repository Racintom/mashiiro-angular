import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Category} from '../../shared-module/model';
import {Observable} from 'rxjs';
import {CategoryService} from '../../shared-module/service/category.service';

@Injectable({
  providedIn: 'root'
})
export class CategoryResolverService implements Resolve<Category[]> {

  constructor(
    private service: CategoryService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Category[]> | Promise<Category[]> | Category[] {
    return this.service.categories$;
  }
}
