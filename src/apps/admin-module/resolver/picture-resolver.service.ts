import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Picture} from '../../shared-module/model';
import {Observable} from 'rxjs';
import {PictureDataService} from '../../shared-module/data-service/picture-data.service';

@Injectable({
  providedIn: 'root'
})
export class PictureResolverService implements Resolve<Picture> {

  constructor(
    private service: PictureDataService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Picture> | Promise<Picture> | Picture {
    return undefined;
  }
}
