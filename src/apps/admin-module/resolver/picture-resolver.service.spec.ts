import {TestBed} from '@angular/core/testing';

import {PictureResolverService} from './picture-resolver.service';

describe('PictureResolverService', () => {
  let service: PictureResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PictureResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
