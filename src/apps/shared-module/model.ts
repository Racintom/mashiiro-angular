export interface ApiResponse<T> {
  code: number;
  message: string;
  data: T;
}

export enum PictureLocation {
  HomepageSlider = 'HomepageSlider',
  Portfolio = 'Portfolio',
  HomepageUnderSlider = 'HomepageUnderSlider',
  AboutMeMainPhoto = 'AboutMeMainPhoto',
  AboutMeUnderMainFirstPhoto = 'AboutMeUnderMainFirstPhoto',
  AboutMeUnderMainSecondPhoto = 'AboutMeUnderMainSecondPhoto',
  PortfolioMainPhoto = 'PortfolioMainPhoto'
}

export enum TextLocation {
  AboutMeMainTextUnderHead = 'AboutMeMainTextUnderHead',
  AboutMeMainTextHead = 'AboutMeMainTextHead',
  AboutMeUnderMainFirstUnderHeadText = 'AboutMeUnderMainFirstUnderHeadText',
  AboutMeUnderMainSecondUnderHeadText = 'AboutMeUnderMainSecondUnderHeadText',
  PortfolioMainTextHead = 'PortfolioMainTextHead'

}

export interface Picture {
  id?: number;
  path: string;
  category?: Category;
  description: string;
  title: string;
  origName: string;
}

export interface Category {
  id?: number;
  name: string;
  pathPortfolioMainPicture?: string;
}

export interface Text {
  id?: number;
  text: string;
}

/*
export interface FilterPayload {
  action: FilterAction;
  filters: CategoryFilter[];
}

export interface ActiveFilter {
  action: FilterAction;
  id: number;
}
*/

export interface CategoryFilter {
  category: Category;
  active: boolean;
}

export enum FormMode {
  ADD = 0,
  EDIT = 1
}
