import {Injectable} from '@angular/core';
import {CategoryDataService} from '../data-service/category-data.service';
import {Observable} from 'rxjs';
import {CategoryFilter} from '../model';
import {map, tap} from 'rxjs/operators';

export const defaultCategoryFilter: CategoryFilter = {
  category: {
    name: 'All',
    id: -1
  },
  active: true
};


@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  readonly categoryFilters$: Observable<CategoryFilter[]> = this.categoryDataService.getCategories().pipe(
    map(categories => categories.map(category => {
      return {
        category,
        active: false
      };
    })),
    // todo: zeptat se toma jak tohle vylepsit
    tap(categoryFilters => categoryFilters.unshift(defaultCategoryFilter))
  );

  constructor(
    private categoryDataService: CategoryDataService
  ) {}

}
