import {Injectable} from '@angular/core';
import {PictureDataService} from '../data-service/picture-data.service';
import {Picture, PictureLocation} from '../model';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PictureService {

  constructor(
    private pictureDataService: PictureDataService
  ) { }

  getPicturesWithPaging(location: PictureLocation, amount: number, offset: number): Observable<Picture[]> {
    return this.pictureDataService.getPicturesByLocation(location, {
      amount,
      offset
    });
  }

  getAllPictures(location: PictureLocation): Observable<Picture[]> {
    return this.pictureDataService.getPicturesByLocation(location);
  }

  getSinglePicture(location: PictureLocation): Observable<Picture> {
    return this.getAllPictures(location).pipe(
      filter(pictures => pictures != null && pictures.length > 0),
      map(pictures => pictures[0])
    );
  }
}
