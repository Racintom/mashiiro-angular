import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';


const routes: Routes = [
  {
    path: 'admin',
    loadChildren: () => import('./admin-module/admin.module').then(module => module.AdminModule)
  },
  {
    path: '',
    pathMatch: 'full',
    loadChildren: () => import('./web-module/web.module').then(module => module.WebModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
