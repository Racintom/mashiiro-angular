import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {WebModule} from './web-module/web.module';
import {AdminModule} from './admin-module/admin.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppComponent} from './app/app.component';
import {StoreModule} from '@ngrx/store';
import {categoryReducer} from './admin-module/store/reducer/category-reducer';
import {EffectsModule} from '@ngrx/effects';
import {CategoryEffects} from './admin-module/store/effect/category-effects';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    WebModule,
    AdminModule,
    BrowserAnimationsModule,
    StoreModule.forRoot({state: categoryReducer}),
    EffectsModule.forRoot([CategoryEffects])
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
