import {Injectable} from '@angular/core';
import {CategoryService} from '../../shared-module/service/category.service';
import {map, switchMap} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {Picture, PictureLocation} from '../../shared-module/model';
import {PictureService} from '../../shared-module/service/picture.service';

export interface LoadPictureEvent {
  amount: number;
  offset: number;
}

@Injectable({
  providedIn: 'root'
})
export class PortfolioService {

  private readonly activeFilterCategoryId$ = new Subject<number>();
  private readonly triggerPictureLoad$ = new Subject<LoadPictureEvent>();
  private readonly mainPictures$: Observable<Picture[]> = this.pictureService.getAllPictures(PictureLocation.PortfolioMainPhoto);

  readonly filteredPictures$: Observable<Picture[]> = this.triggerPictureLoad$.pipe(
    switchMap((payload: LoadPictureEvent) =>
      this.pictureService.getPicturesWithPaging(PictureLocation.Portfolio, payload.amount, payload.offset)
  ));

  readonly currentMainPicture$: Observable<Picture> = this.activeFilterCategoryId$.pipe(
    switchMap(currentlySelectedCategoryId => this.mainPictures$.pipe(
      map(picture => picture.find(pic => pic.category.id === currentlySelectedCategoryId))
    ))// todo: add default value
  );

  constructor(
    private categoryService: CategoryService,
    private pictureService: PictureService,

  ) {}

  setFilter(newlySelectedCategoryId: number): void {
    this.activeFilterCategoryId$.next(newlySelectedCategoryId);
  }

  loadMorePictures(payload: LoadPictureEvent): void {
    this.triggerPictureLoad$.next(payload);
  }
}
