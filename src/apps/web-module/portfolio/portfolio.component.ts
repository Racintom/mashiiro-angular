import {Component, OnInit} from '@angular/core';
import {MainMenuService, Page} from '../shared/component/main-menu/main-menu.service';
import {Observable} from 'rxjs';
import {Picture} from '../../shared-module/model';
import {PortfolioService} from './portfolio.service';

@Component({
  selector: 'ms-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {

  private readonly amount = 10;
  private offset = 0;

  readonly currentPicture$: Observable<Picture> = this.portfolioService.currentMainPicture$;
  readonly pictures$: Observable<Picture[]> = this.portfolioService.filteredPictures$;

  constructor(
    private mainMenuService: MainMenuService,
    private portfolioService: PortfolioService
  ) { }

  ngOnInit() {
    this.mainMenuService.currentPage$.next(Page.PORTFOLIO);
  }

  loadMorePictures(): void {
    this.offset += this.amount;
    this.portfolioService.loadMorePictures({
      amount: this.amount,
      offset: this.offset
    });
  }

  setCurrentFilter(categoryId: number): void {
    this.portfolioService.setFilter(categoryId);
  }
}
