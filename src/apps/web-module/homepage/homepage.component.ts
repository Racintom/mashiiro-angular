import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {PictureDataService} from '../../shared-module/data-service/picture-data.service';
import {Picture, PictureLocation} from '../../shared-module/model';
import {MainMenuService, Page} from '../shared/component/main-menu/main-menu.service';


@Component({
  selector: 'ms-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  readonly sliderPictures$: Observable<Picture[]> = this.pictureDataService.getPicturesByLocation(PictureLocation.HomepageSlider);
  readonly underSliderPicture$: Observable<Picture[]> = this.pictureDataService.getPicturesByLocation(PictureLocation.HomepageUnderSlider);

  constructor(
    private pictureDataService: PictureDataService,
    private mainMenuService: MainMenuService
  ) { }

  ngOnInit(): void {
    this.mainMenuService.currentPage$.next(Page.HOMEPAGE);
  }


}
