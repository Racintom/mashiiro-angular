import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

export enum Page {
  HOMEPAGE = 'Úvod',
  ABOUT_ME = 'O mně',
  PORTFOLIO = 'Portfolio'
}

@Injectable({
  providedIn: 'root'
})
export class MainMenuService {

  readonly currentPage$ = new BehaviorSubject<Page>(Page.HOMEPAGE);

  constructor() { }
}
