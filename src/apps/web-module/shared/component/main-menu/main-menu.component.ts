import {Component} from '@angular/core';
import {MainMenuService} from './main-menu.service';

@Component({
  selector: 'ms-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent {

  openedMenu = false;
  readonly currentPage$ = this.service.currentPage$;

  constructor(
    private service: MainMenuService,
  ) { }

  toggleMenu(): void {
    this.openedMenu = !this.openedMenu;
  }

}
