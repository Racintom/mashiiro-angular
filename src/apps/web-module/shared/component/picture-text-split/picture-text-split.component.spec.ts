import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PictureTextSplitComponent} from './picture-text-split.component';

describe('PictureTextSplitComponent', () => {
  let component: PictureTextSplitComponent;
  let fixture: ComponentFixture<PictureTextSplitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PictureTextSplitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PictureTextSplitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
