import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Category, Picture, Text} from '../../../../shared-module/model';

@Component({
  selector: 'ms-picture-text-split',
  templateUrl: './picture-text-split.component.html',
  styleUrls: ['./picture-text-split.component.scss']
})
export class PictureTextSplitComponent {

  selectedCategoryId: number;

  @Input()
  picture: Picture;

  @Input()
  title: Text;

  @Input()
  description: Text;

  @Input()
  categories: Category[];

  @Output()
  currentFilter = new EventEmitter<number>();

  constructor() { }

  selectedCategory(categoryId: number): void {
    this.selectedCategoryId = categoryId;
    this.currentFilter.emit(categoryId);
  }

}
