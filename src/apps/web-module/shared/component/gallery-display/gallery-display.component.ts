import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Picture} from '../../../../shared-module/model';

@Component({
  selector: 'ms-gallery-display',
  templateUrl: './gallery-display.component.html',
  styleUrls: ['./gallery-display.component.scss']
})
export class GalleryDisplayComponent implements OnInit {

  @Output()
  displayMore = new EventEmitter();

  @Input()
  pictures: Picture[];

  constructor() { }

  ngOnInit(): void {
  }

  loadMore(): void {
    this.displayMore.emit();
  }

}
