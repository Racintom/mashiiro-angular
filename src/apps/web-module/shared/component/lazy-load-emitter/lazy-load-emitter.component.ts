import {Component, ElementRef, EventEmitter, HostListener, Input, Output} from '@angular/core';
import {bufferTime} from 'rxjs/operators';

@Component({
  selector: 'ms-lazy-load-emitter',
  templateUrl: './lazy-load-emitter.component.html',
  styleUrls: ['./lazy-load-emitter.component.scss']
})
export class LazyLoadEmitterComponent { // todo: otestovat, ze to funguje a ze to neemituje casteji, nez za 300ms

  readonly TIME_AFTER_WHICH_EMITS = 300;

  @Input()
  active: boolean;

  @Output()
  visible = new EventEmitter<boolean>();

  constructor(
    private elementRef: ElementRef<HTMLElement>
  ) {
    this.visible.pipe(
      bufferTime(this.TIME_AFTER_WHICH_EMITS)
    );
  }

  @HostListener('scroll', ['$event'])
  onScroll(): void {
    if (this.active && this.amIVisibleOnPage()) {
      this.visible.next();
    }
  }

  private amIVisibleOnPage(): boolean {
    const rect = this.elementRef.nativeElement.getBoundingClientRect();
    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /* or $(window).height() */
      rect.right <= (window.innerWidth || document.documentElement.clientWidth) /* or $(window).width() */
    );
  }
}
