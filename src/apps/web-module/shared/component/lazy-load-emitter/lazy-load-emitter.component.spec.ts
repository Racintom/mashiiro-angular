import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LazyLoadEmitterComponent} from './lazy-load-emitter.component';

describe('LazyLoadEmitterComponent', () => {
  let component: LazyLoadEmitterComponent;
  let fixture: ComponentFixture<LazyLoadEmitterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LazyLoadEmitterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LazyLoadEmitterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
