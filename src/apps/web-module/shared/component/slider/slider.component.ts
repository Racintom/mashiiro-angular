import {Component, Input} from '@angular/core';
import {Picture} from '../../../../shared-module/model';

@Component({
  selector: 'ms-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
})
export class SliderComponent {
  // todo: pridat resolution service kvuli zmene poctu viditelnych obrazku
  private readonly interval = 4500; // in ms
  readonly animationDuration = 250; // in ms
  readonly options = {
    loop: true,
    autoplay: false,
    nav: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    freeDrag: false,
    lazyLoad: true,
    lazyLoadEager: 1,
    animateIn: 'slideInRight',
    animateOut: 'slideOutLeft',
    responsive: {
      400: {
        items: 1  // todo: tady je bug. Kdyz je jen jeden item, animace musi byt jina bo implementace
      },
      768: {
        items: 2
      },
      1024: {
        items: 3
      }
    }
  };

  @Input()
  set photos(value: Picture[]) {
    this.pictures = value;
  }
  pictures: Picture[];
}
