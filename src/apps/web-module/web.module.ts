import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomepageComponent} from './homepage/homepage.component';
import {AboutComponent} from './about/about.component';
import {PortfolioComponent} from './portfolio/portfolio.component';
import {WebRoutingModule} from './web-routing.module';
import {FooterComponent} from './shared/component/footer/footer.component';
import {SliderComponent} from './shared/component/slider/slider.component';
import {HttpClientModule} from '@angular/common/http';
import {LazyImageComponent} from './shared/component/lazy-image/lazy-image.component';
import {BackgroundImageDirective} from './shared/directive/background-image.directive';
import {CarouselModule} from 'ngx-owl-carousel-o';
import {MainComponent} from './main/main.component';
import {MainMenuComponent} from './shared/component/main-menu/main-menu.component';
import {PictureTextSplitComponent} from './shared/component/picture-text-split/picture-text-split.component';
import {LazyLoadEmitterComponent} from './shared/component/lazy-load-emitter/lazy-load-emitter.component';
import {SharedModule} from '../shared-module/shared.module';
import {GalleryDisplayComponent} from './shared/component/gallery-display/gallery-display.component';


@NgModule({
  declarations: [MainMenuComponent, HomepageComponent, AboutComponent,
    PortfolioComponent, FooterComponent, SliderComponent, LazyImageComponent,
    BackgroundImageDirective,
    MainComponent,
    PictureTextSplitComponent,
    LazyLoadEmitterComponent,
    GalleryDisplayComponent,
    ],
  exports: [
    LazyImageComponent
  ],
  imports: [
    CommonModule,
    WebRoutingModule,
    HttpClientModule,
    CarouselModule,
    SharedModule
  ]
})
export class WebModule { }
