import {Component, OnInit} from '@angular/core';
import {MainMenuService, Page} from '../shared/component/main-menu/main-menu.service';
import {PictureDataService} from '../../shared-module/data-service/picture-data.service';
import {PictureLocation, TextLocation} from '../../shared-module/model';
import {TextDataService} from '../../shared-module/data-service/text-data.service';
import {PictureService} from '../../shared-module/service/picture.service';

@Component({
  selector: 'ms-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  readonly mainPicture$ = this.pictureService.getSinglePicture(PictureLocation.AboutMeMainPhoto);
  readonly pictureFirst$ = this.pictureService.getSinglePicture(PictureLocation.AboutMeUnderMainFirstPhoto);
  readonly pictureSecond$ = this.pictureService.getSinglePicture(PictureLocation.AboutMeUnderMainSecondPhoto);
  readonly title$ = this.textDataService.getTextByLocation(TextLocation.AboutMeMainTextHead);
  readonly description$ = this.textDataService.getTextByLocation(TextLocation.AboutMeMainTextUnderHead);

  constructor(
    private mainMenuService: MainMenuService,
    private pictureDataService: PictureDataService,
    private textDataService: TextDataService,
    private pictureService: PictureService
  ) { }

  ngOnInit() {
    this.mainMenuService.currentPage$.next(Page.ABOUT_ME);
  }

}
