import {Injectable} from '@angular/core';
import {BaseDataService} from './base-data.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Text, TextLocation} from '../model';
import {map} from 'rxjs/operators';
import {TextAdapter} from '../../libs/adapter/text-adapter';

@Injectable({
  providedIn: 'root'
})
export class TextDataService extends BaseDataService {

  constructor(httpClient: HttpClient) {
    super(httpClient);
  }

  getTextByLocation(location: TextLocation): Observable<Text> {
    return this.get<Text>(`text/${location}`).pipe(
      map(text => TextAdapter.adapt(text)),
    );
  }
}
