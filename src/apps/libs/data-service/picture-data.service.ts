import {Injectable} from '@angular/core';
import {BaseDataService} from './base-data.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Picture, PictureLocation} from '../model';
import {map} from 'rxjs/operators';
import {PictureAdapter} from '../../libs/adapter/picture-adapter';

@Injectable({
  providedIn: 'root'
})
export class PictureDataService extends BaseDataService {

  constructor(httpClient: HttpClient) {
    super(httpClient);
  }

  getPictureById(id: number): Observable<Picture> {
    return this.get<Picture>(`picture/${id}`).pipe(
      map(picture => PictureAdapter.adapt(picture))
    );
  }

  // todo: add paging
  getPicturesByLocation(location: PictureLocation, body?: object): Observable<Picture[]> {
    return this.post<Picture[]>(`picture/${location}`, body).pipe(
      map(pictures => pictures.map(picture => PictureAdapter.adapt(picture)))
    );
  }


}
