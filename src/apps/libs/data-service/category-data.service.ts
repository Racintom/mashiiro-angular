import {Injectable} from '@angular/core';
import {BaseDataService} from './base-data.service';
import {HttpClient} from '@angular/common/http';
import {Category} from '../model';
import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {CategoryAdapter} from '../../libs/adapter/category-adapter';

@Injectable({
  providedIn: 'root'
})
export class CategoryDataService extends BaseDataService {

  constructor(httpClient: HttpClient) {
    super(httpClient);
  }

  getCategories(): Observable<Category[]> {
    return this.get<Category[]>(`category`).pipe(
      map(categories => categories.map(category => CategoryAdapter.adapt(category))),
    );
  }

  deleteCategory(id: number): Observable<boolean> {
    return this.delete<boolean>(`category/${id}`).pipe(
      catchError((err) => {
        console.error(err);
        return of(false);
      }),
      map(() => true)
    );
  }

  updateCategory(category: Category): Observable<boolean> {
    return this.put<boolean>(`category/${category.id}`, category).pipe(
      catchError((err) => {
        console.error(err);
        return of(false);
      }),
      map(() => true)
    );
  }

  addCategory(category: Category): Observable<string> {
    return this.post<boolean>(`category/`, category, this.formDataOptions).pipe(
      tap(res => console.log(res)),
      catchError((err) => {
        console.error(err);
        return of(null);
      }),
    );
  }
}
