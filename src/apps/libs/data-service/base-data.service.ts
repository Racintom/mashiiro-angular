import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

type Adapter<T> = (response) => T;

@Injectable({
  providedIn: 'root'
})
export class BaseDataService {

  private readonly options = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json; charset=UTF-8"'
    })
  };

  protected readonly formDataOptions = {
    headers: new HttpHeaders({
      Accept: 'application/json'
    })
  };

  private readonly baseUrl = '/api/';

  constructor(
    private httpClient: HttpClient,
  ) {}

  protected get<T>(url: string): Observable<T> {
    return this.httpClient.get<T>(`${this.baseUrl}${url}`, this.options);
  }

  protected post<T>(url: string, body?: any, defaultHttpOptions = this.options): Observable<T> {
    return this.httpClient.post<T>(`${this.baseUrl}${url}`, body, defaultHttpOptions);
  }

  protected put<T>(url: string, body?: any, noHttpOptions = false): Observable<T> {
    return this.httpClient.put<T>(`${this.baseUrl}${url}`, body, noHttpOptions ? undefined : this.options);
  }

  protected delete<T>(url: string): Observable<T> {
    return this.httpClient.delete<T>(this.baseUrl + url, this.options);
  }


}
