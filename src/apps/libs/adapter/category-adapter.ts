import {Category} from '../model';

export class CategoryAdapter {
  static adapt(o: any): Category {
    return {
      id: o?.ID,
      name: o?.name,
    };
  }
}
