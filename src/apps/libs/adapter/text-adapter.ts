import {Text} from '../model';

export class TextAdapter {
  static adapt(o: any): Text {
    console.log(o);
    return {
      id: o.ID,
      text: o.text
    };
  }
}
