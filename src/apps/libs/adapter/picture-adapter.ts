import {Picture} from '../model';
import {CategoryAdapter} from './category-adapter';

export class PictureAdapter {
  static adapt(o: any): Picture {
    return {
      id: o.id,
      path: o.path,
      category: CategoryAdapter.adapt(o.category),
      title: o.title,
      description: o.description,
      origName: o.origName
    };
  }
}
