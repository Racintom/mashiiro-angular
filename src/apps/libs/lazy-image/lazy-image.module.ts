import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LazyImageComponent} from './lazy-image.component';



@NgModule({
  declarations: [
    LazyImageComponent
  ],
  exports: [
    LazyImageComponent
  ],
  imports: [
    CommonModule
  ]
})
export class LazyImageModule { }
