import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'ms-lazy-image',
  templateUrl: './lazy-image.component.html',
  styleUrls: ['./lazy-image.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LazyImageComponent {
// todo: bude napojene na resolution service, ktera bude setovat suffix dle potreby
  @Input()
  src: string;

  @Input()
  alt: string;

  loaded() {
    // this.src = this.lazySrc;
  }
}
